import React, { Component } from 'react';
import './UrlInput.css';

class UrlInput extends Component {

  render() {
    return <>
        <label className='UrlInput'>Url:
          <input type="text" value={this.props.text} onChange={(e) => this.props.onChange(e.target.value)}></input>
        </label>
    </>;
  }
}

export default UrlInput;
