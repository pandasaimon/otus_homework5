import './ResultWindow.css';

export default function ResultWindow({result}) {
  return <div className={result.status}>{result.text}</div>;
}
