export default function MyButton({onClick, caption}) {
  return <button onClick={onClick}>{caption}</button>;
}
