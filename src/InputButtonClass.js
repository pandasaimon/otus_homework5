import React, { Component } from 'react';
import './UrlInput.css';

export default class InputButtonClass extends Component {

  render() {
    return <>
		<label className='UrlInput'>Url:
		<input type="text" value={this.props.value} onChange={(e) => this.props.onValueChange(e.target.value)}></input>
		</label>
		<button onClick={this.props.onClick}>{this.props.caption}</button>
    </>;
  }
}