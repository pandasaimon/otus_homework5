import axios from 'axios';

export const RESPONSE_STATE = { NONE: 'none', WAIT: 'wait', OK: 'ok', ERROR: 'error' };

export default class RequestHelper {

    static async fetchRequest(inputUrlText) {
        try {
            let response = await axios.get(inputUrlText);
            return {
                status: RESPONSE_STATE.OK,
                text: JSON.stringify(response.data)
            };
        } catch (err) {
            let errMessage = `Ошибка: ${RequestHelper.HttpErrorHandler(err)}`;
            return {
                status: RESPONSE_STATE.ERROR,
                text: errMessage
            };
        }
    }

    static HttpErrorHandler(error) {
        let errorText = "Ошибка при выполнении запроса.";
        if (error === null) {
            errorText = "Что-то пошло ни так. Данные об ошибке не найдены";
        } else if (axios.isAxiosError(error)) {
            console.log(error);
            if (error.response) {
                errorText += ` Статус ошибки ${error.response?.status}`;
            } else if (error.request) {
                errorText = error.message;
            } else {
                errorText = error.message;
            }
            return errorText;
        }
    }
}