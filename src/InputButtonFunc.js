import './UrlInput.css';

export default function InputButtonFunc({value, onValueChange, onClick, caption}) {
	return <>
		<label className='UrlInput'>Url:
		<input type="text" value={value} onChange={(e) => onValueChange(e.target.value)}></input>
		</label>
		<button onClick={onClick}>{caption}</button>
	</>;
  }