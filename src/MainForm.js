import React, { useState } from 'react';
import MyButton from './MyButton';
import UrlInput from './UrlInput';
import InputButtonFunc from './InputButtonFunc';
import InputButtonClass from './InputButtonClass';
import ResultWindow from './ResultWindow';
import RequestHelper, { RESPONSE_STATE } from './RequestHelper'

export default function MainForm() {

  const [url, setUrl] = useState('https://catfact.ninja/fact');
  const [response, setResponse] = useState({
    status: RESPONSE_STATE.NONE,
    text: "Здесь будет отображен результат"
  });

  async function handleClick() {
      if(url != "") {
        setResponse(await RequestHelper.fetchRequest(url));
      } else {
        setResponse({
          status: RESPONSE_STATE.ERROR,
          text: "Url - обязательный параметр"
        })
      }
  }

  return <>
    <div>
      <p>Новая версия с функцией:</p>
      <InputButtonFunc value={url} onValueChange={setUrl} onClick={handleClick} caption="Отправить" />
    </div>
    <div>
      <p>Новая версия с классом:</p>
      <InputButtonClass value={url} onValueChange={setUrl} onClick={handleClick} caption="Отправить" />
    </div>
    <div>
      <p>Старая версия:</p>  
      <UrlInput text={url} onChange={setUrl} />
      <MyButton onClick={handleClick} caption="Отправить"/>
    </div>
    <ResultWindow result={response}/>
  </>;
}

